package com.springexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class ProductController {
    @Autowired
    private IProductService productService;

    //mapping the getProduct() method to /product
    @GetMapping(value = "/product")
    public List<Product> getProduct() {
    //finds all the products
        List<Product> products = productService.findAll();
    //returns the product list
        return products;
    }

    @GetMapping(value = "/")
    public ArrayList<String> Hello(){
        ArrayList<String> stringArrayList =new ArrayList<String>();
        stringArrayList.add("Your Application Works Successfully.... QA");
        return stringArrayList;
    }
}
